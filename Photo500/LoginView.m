//
//  LoginViewController.m
//  Photo500
//
//  Created by Aleksander Posobiec on 14/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import "LoginView.h"
#import "PXFetcher.h"
@interface LoginView() <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginView
@synthesize loggedIn;

- (IBAction)finishedUserName:(UITextField *)sender {
    NSLog(@"Weszlo");
    [self.passField.window makeKeyAndVisible];
    
}
- (IBAction)finishedPassword:(UITextField *)sender {
    NSLog(@"Dalej");
    [self resignFirstResponder];
    [self.loginButton.window makeKeyAndVisible];
}


- (IBAction)loginTouchedUp:(id)sender {

    
    NSString* login = _loginField.text;
    NSString* pass =_passField.text;
    
    self.loggedIn = [self authenticateUser: login withPassword: pass];
    
    if(loggedIn)
        [self removeFromSuperview];
    
}

-(BOOL) authenticateUser: (NSString*) login withPassword: (NSString*) pass {
   
    /* 
     
     
     
     */
    
    return YES;
    
}




@end
