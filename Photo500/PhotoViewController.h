//
//  ViewController.h
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController
@property (nonatomic, strong) NSURL* imageURL;

+ (PhotoViewController *)photoViewController;
@end

