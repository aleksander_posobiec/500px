//
//  FeaturedViewController.m
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import "FeaturedViewController.h"
#import "PhotoViewController.h"
#import "CustomTable.h"
#import "PXFetcher.h"
#import "CustomCell.h"
#import "LoginView.h"

@interface FeaturedViewController() <MyCustomTableViewDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet CustomTable *resultTable;
@end


@implementation FeaturedViewController

-(void) viewDidLoad
{
    [super viewDidLoad];
    CustomTable *customTable = _resultTable;
    customTable.myDelegate = self;
    [_segment addTarget:self
                         action:@selector(selectionChanged)
               forControlEvents:UIControlEventValueChanged];
    [self showAnimation];
    [self.resultTable loadPhotos:1 withTerms:PX_POPULAR];
    
}
-(void)showAnimation {
    LoginView *loginView = [[[NSBundle mainBundle] loadNibNamed:@"LoginView"  owner:self options:nil] lastObject];
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    [self.view addSubview: loginView];
    loginView.frame = CGRectMake(0, -loginView.frame.size.width, loginView.frame.size.width, loginView.frame.size.height);
    [UIView animateWithDuration:1.0
                     animations:^{
                         loginView.frame = CGRectMake(0,0,loginView.frame.size.width, loginView.frame.size.height);}];
      [mainWindow addSubview: loginView];
}


-(void) selectionChanged
{
    [self.resultTable.photos removeAllObjects];
    switch ([_segment selectedSegmentIndex]) {
        case 0:
            [self.resultTable loadPhotos:1 withTerms:PX_POPULAR];
            break;
        case 1:
            [self.resultTable loadPhotos:1 withTerms:PX_UPCOMING];
            break;
        case 2:
            [self.resultTable loadPhotos:1 withTerms:PX_EDITORS];
            break;
        default:
            [self.resultTable loadPhotos:1 withTerms:PX_POPULAR];
    }
    [self.resultTable setContentOffset:CGPointZero animated:YES];
}




- (void)customTableView:(CustomTable *)tableView didSelectPhoto:(NSDictionary *)photo
{
    PhotoViewController *photoViewController = [PhotoViewController photoViewController];
    photoViewController.imageURL = [PXFetcher URLForPhoto:photo[PX_RESULTS_PHOTO]];
    photoViewController.navigationController.navigationBar.topItem.title = [photo valueForKey:PX_RESULTS_NAME];
    [self showViewController:photoViewController sender:nil];
}



@end
