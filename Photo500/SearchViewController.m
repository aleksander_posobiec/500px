//
//  SearchViewController.m
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import "SearchViewController.h"
#import "CustomTable.h"
#import "PXFetcher.h"
#import "PhotoViewController.h"
@interface SearchViewController()<UISearchBarDelegate,MyCustomTableViewDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet CustomTable *resultTable;

@end

@implementation SearchViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    CustomTable *customTable = _resultTable;
    customTable.myDelegate = self;
    self.searchBar.delegate = self;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self.resultTable loadPhotos:1 withTerms:searchBar.text];
}

- (void)customTableView:(CustomTable *)tableView didSelectPhoto:(NSDictionary *)photo
{
    PhotoViewController *photoViewController = [PhotoViewController photoViewController];
    photoViewController.navigationController.navigationBar.topItem.title = [photo valueForKey:PX_RESULTS_NAME];
    photoViewController.imageURL = [PXFetcher URLForPhoto:photo[PX_RESULTS_PHOTO]];
    [self showViewController:photoViewController sender:nil];
}

@end
