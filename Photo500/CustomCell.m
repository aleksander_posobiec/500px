//
//  CustomCell.m
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import "CustomCell.h"

@interface CustomCell()


@end

@implementation CustomCell
@synthesize image = _image;
@synthesize name = _name;
@synthesize desc = _desc;


@end
