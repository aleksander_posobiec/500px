//
//  LoginViewController.h
//  Photo500
//
//  Created by Aleksander Posobiec on 14/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface LoginView : UIView <UITextFieldDelegate>
@property (nonatomic) BOOL loggedIn;


@end
