//
//  CustomTable.m
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import "CustomTable.h"
#import "CustomCell.h"
#import "PXFetcher.h"
#import "FeaturedViewController.h"

@interface CustomTable() <UITableViewDataSource,UITableViewDelegate>
@property (weak,nonatomic) NSString* terms;
@end

@implementation CustomTable
@synthesize myDelegate;


-(void) loadPhotos:(int)page withTerms: (NSString*) term
{
    if([term isEqualToString:@"Upcoming"]){
        NSLog(@"Upcoming");
        [self downloadPhotos:[PXFetcher URLForUpcomingQuery:page]];
    }
    else if([term isEqualToString:@"Editors"]){
        NSLog(@"Editors");
        [self downloadPhotos:[PXFetcher URLForEditorsQuery:page]];
    }
    else if([term isEqualToString:@"Popular"]){
        NSLog(@"Popular");
        [self downloadPhotos:[PXFetcher URLForPopularQuery:page]];
    }
    else {
        NSLog(@"Search");
          _photos = [[NSMutableArray alloc] init];
        [self downloadPhotos:[PXFetcher URLForSearchQuery:term :page]];
    }
    _terms = term;
    
}


-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self registerNib:[UINib nibWithNibName:@"cell" bundle:nil] forCellReuseIdentifier:@"CustomCell"];
        self.dataSource = self;
        self.delegate = self;
    }
    return self;
}

- (void) setPhotos:(NSMutableArray *)photos
{
    if(_photos)
        [_photos addObjectsFromArray: photos];
    else {
        _photos = photos;
    }
    [self reloadData];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomCell *cell = (CustomCell *)[self dequeueReusableCellWithIdentifier:@"CustomCell" forIndexPath:indexPath];
    if([_photos count] > 0){
    NSDictionary *photo = _photos[indexPath.row];
    if(photo) {
        cell.image.image = nil;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[photo valueForKey:PX_RESULTS_PHOTO]]];
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.image.image = image;
                    [cell setNeedsLayout];
                });
            }
        });
        
        if(![[photo valueForKey:PX_RESULTS_DESC] isEqual:[NSNull null]] )
            cell.desc.text = [photo valueForKey:PX_RESULTS_DESC];
        
        cell.name.text = [photo valueForKey:PX_RESULTS_NAME];
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.photos count];
}

-(void)tableView:(UITableView*) tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.myDelegate respondsToSelector:@selector(customTableView:didSelectPhoto:)]) {
        NSDictionary *photo = self.photos[indexPath.row];
        [self.myDelegate customTableView:self didSelectPhoto:photo];
    }
}

-(void) tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.photos count] - indexPath.row <=3){
        _page++;
        [self loadPhotos:_page withTerms:_terms];
        
    }
}

-(void) downloadPhotos: (NSURL*) query
{
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:query
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                NSData *jsonResults = [NSData dataWithContentsOfURL: query];
                NSDictionary *propertyListResults = [NSJSONSerialization JSONObjectWithData:jsonResults options:0 error:NULL];
                NSMutableArray *photos = [propertyListResults valueForKeyPath:PX_RESULTS_PHOTOS];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //jesli po raz pierwszy jest odwolanie to ja zainicjuje
                    if(!_photos){
                        _photos = [[NSMutableArray alloc] init];
                    }
                    [self setPhotos:photos];
                    
                    
                });
            }] resume];
}

@end
