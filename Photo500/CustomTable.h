//
//  CustomTable.h
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyCustomTableViewDelegate;

@interface CustomTable : UITableView

@property (nonatomic) int page;
@property (strong,nonatomic) NSMutableArray* photos;//of NSDictionaries from JSON
@property (weak,nonatomic) id<MyCustomTableViewDelegate> myDelegate;
-(void) loadPhotos:(int) page withTerms: (NSString*) term;

@end

@protocol MyCustomTableViewDelegate <NSObject>
@optional
- (void)customTableView:(CustomTable *)tableView didSelectPhoto:(NSDictionary *)photo;

@end
