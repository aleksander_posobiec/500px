//
//  PXFetcher.h
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CustomTable.h"


//lepiej robic na const 
#define CUSTOMER_KEY @"sMYXTaUJYx8mfFvFWVgQx8Jx8F1hvWy1Jq7mlicR"
#define CUSTOMER_SECRET @"g278EEocbThGNCeiGuUxYRQxgPcpBGb87QLAYwP2"

#define PX_RESULTS_PHOTOS @"photos"
#define PX_RESULTS_NAME @"name"
#define PX_RESULTS_DESC @"description"
#define PX_RESULTS_PHOTO @"image_url"

#define PX_POPULAR @"Popular"
#define PX_UPCOMING @"Upcoming"
#define PX_EDITORS @"Editors"

@interface PXFetcher : NSObject
+ (NSURL *)URLForSearchQuery: (NSString *) term : (int) page;
+ (NSURL *)URLForUpcomingQuery: (int) page;
+ (NSURL *)URLForPopularQuery: (int) page;
+ (NSURL *)URLForEditorsQuery: (int) page;
+ (NSURL *)URLForPhoto: (NSString*) photo;

@end