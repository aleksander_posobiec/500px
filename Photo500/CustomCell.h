//
//  CustomCell.h
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *desc;

@end
