//
//  PXFetcher.m
//  Photo500
//
//  Created by Aleksander Posobiec on 09/12/15.
//  Copyright © 2015 Aleksander Posobiec. All rights reserved.
//

#import "PXFetcher.h"
#import "CustomTable.h"
@implementation PXFetcher

+ (NSURL *)URLForSearchQuery: (NSString *) term : (int) page
{
    NSString *query;
    query = [NSString stringWithFormat:@"https://api.500px.com/v1/photos/search?term=%@&page=%d&nsfw=1&image_size=5&consumer_key=%@",term,page, CUSTOMER_KEY];
    return [NSURL URLWithString:query];
}
+ (NSURL *)URLForEditorsQuery: (int) page
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://api.500px.com/v1/photos?feature=editors&sort=created_at&image_size=3&page=%d&include_store=store_download&include_states=voted&consumer_key=%@", page, CUSTOMER_KEY]];
}

+ (NSURL *)URLForPopularQuery: (int) page
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://api.500px.com/v1/photos?feature=popular&sort=created_at&image_size=3&page=%d&include_store=store_download&include_states=voted&consumer_key=%@",page, CUSTOMER_KEY]];
}

+ (NSURL *)URLForUpcomingQuery: (int) page
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://api.500px.com/v1/photos?feature=upcoming&sort=created_at&image_size=3&page=%d&include_store=store_download&include_states=voted&consumer_key=%@", page,CUSTOMER_KEY]];
}

+(NSURL*) URLForPhoto: (NSString *) photoURL
{
    return [NSURL URLWithString:photoURL];
}




@end
